from django.apps import AppConfig


class TragoConfig(AppConfig):
    name = 'trago'
