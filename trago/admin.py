from django.contrib import admin
from trago.models import *

# Register your models here.
admin.site.register(Country)
admin.site.register(Destination)
admin.site.register(Places)
admin.site.register(Transportation)
admin.site.register(Hotel)
admin.site.register(Trip)
admin.site.register(Booking)
admin.site.register(Companies)
admin.site.register(Services)
admin.site.register(CompanyServices)
admin.site.register(Itinerary)
