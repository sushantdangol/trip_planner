from django.db import models
from django.conf import settings

class Country(models.Model):
    country_name = models.CharField(max_length=255, default=None, blank=False)
    country_code = models.IntegerField()

class Destination(models.Model):
    destination_title = models.CharField(max_length=255)
    destination_description = models.TextField()
    destination_images = models.ImageField()
    is_active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now=True)
    updated_at = models.DateTimeField(auto_now=True)

class Places(models.Model):
    PLACE_TYPE = [
        ()
    ]

    destination = models.ForeignKey(Destination, on_delete=models.CASCADE)
    place_title = models.CharField(max_length=255)
    place_description = models.TextField()
    place_type = models.CharField(max_length=255)
    place_attributes = models.CharField(max_length=255)
    created_at = models.DateTimeField(auto_now=True)
    updated_at = models.DateTimeField(auto_now=True)

class Transportation(models.Model):
    destination = models.ForeignKey(Destination, on_delete=models.CASCADE)
    transportation_name = models.CharField(max_length=255, blank=True)
    transportation_contact = models.CharField(max_length=20)
    transportation_type = models.CharField(max_length=255)
    transportation_cost = models.IntegerField()
    created_at = models.DateTimeField(auto_now=True)
    updated_at = models.DateTimeField(auto_now=True)

class Hotel(models.Model):
    destination = models.ForeignKey(Destination, on_delete=models.CASCADE)
    hotel_name = models.CharField(max_length=255)
    hotel_email = models.EmailField()
    hotel_address = models.TextField()
    hotel_contact = models.CharField(max_length=20)
    hotel_cost = models.IntegerField()
    hotel_attribute = models.CharField(max_length=255)
    created_at = models.DateTimeField(auto_now=True)
    updated_at = models.DateTimeField(auto_now=True)

class Trip(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    trip_name = models.CharField(max_length=150)
    trip_description = models.TextField()
    group_size = models.IntegerField()
    estimated_price = models.IntegerField()
    from_date = models.DateField()
    to_date = models.DateField()
    status = models.BooleanField(default=False)
    attributes = models.CharField(max_length=255)
    created_at = models.DateTimeField(auto_now=True)
    updated_at = models.DateTimeField(auto_now=True)

class Booking(models.Model):
    trip = models.ForeignKey(Trip, on_delete=models.CASCADE)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    booking_no = models.IntegerField()
    booking_price = models.FloatField()
    status = models.BooleanField(default=False)
    address = models.TextField()
    created_at = models.DateTimeField(auto_now=True)
    updated_at = models.DateTimeField(auto_now=True)

class Companies(models.Model):
    company_name = models.CharField(max_length=255)
    company_email = models.EmailField(blank=True)
    company_website = models.URLField()
    company_contact = models.CharField(max_length=20)
    created_at = models.DateTimeField(auto_now=True)
    updated_at = models.DateTimeField(auto_now=True)

class Services(models.Model):
    SERVICE_TYPE = [
        ()
    ]

    service_name = models.CharField(max_length=255)

class CompanyServices(models.Model):
    company = models.ForeignKey(Companies, on_delete=models.CASCADE)
    service = models.ForeignKey(Services, on_delete=models.CASCADE)

class Itinerary(models.Model):
    activity_type = models.CharField(max_length=255)
    place = models.ForeignKey(Places, on_delete=models.CASCADE)
    hotel = models.ForeignKey(Hotel, on_delete=models.CASCADE)
    transportation = models.ForeignKey(Transportation, on_delete=models.CASCADE)
    starts_at = models.DateField()
    ends_at = models.DateField()
    estimated_cost = models.FloatField()
    trip = models.ForeignKey(Trip, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now=True)
    updated_at = models.DateTimeField(auto_now=True)