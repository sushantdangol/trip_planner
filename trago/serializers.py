from rest_framework import serializers
from trago.models import *

class CountrySerializer(serializers.ModelSerializer):

    class Meta:
        model = Country
        fields = '__all__'

class DestinationSerializer(serializers.ModelSerializer):

    class Meta:
        model = Destination
        fields = '__all__'

class PlacesSerializer(serializers.ModelSerializer):

    class Meta:
        model = Places
        fields = '__all__'

class TransportaionSerializer(serializers.ModelSerializer):

    class Meta:
        model = Transportation
        fields = '__all__'

class HotelSerializer(serializers.ModelSerializer):

    class Meta:
        model = Hotel
        fields = '__all__'

class TripSerializer(serializers.ModelSerializer):

    class Meta:
        model = Trip
        fields = '__all__'

class BookingSerializer(serializers.ModelSerializer):

    class Meta:
        model = Booking
        fields = '__all__'

class CompaniesSerializer(serializers.ModelSerializer):

    class Meta:
        model = Companies
        fields = '__all__'

class ServicesSerializer(serializers.ModelSerializer):

    class Meta:
        model = Services
        fields = '__all__'

class CompanyServicesSerializer(serializers.ModelSerializer):

    class Meta:
        model = CompanyServices
        fields = '__all__'

class ItinerarySerializer(serializers.ModelSerializer):

    class Meta:
        model = Itinerary
        fields = '__all__'

