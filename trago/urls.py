from django.urls import path, include
from rest_framework import routers
from trago.viewset import *

router = routers.SimpleRouter()

router.register('country', CountryViewSet)
router.register('destination', DestinationViewSet)
router.register('places', PlacesViewSet)
router.register('transportation', TransportaionViewSet)
router.register('hotel', HotelViewSet)
router.register('trip', TripViewSet)
router.register('booking', BookingViewSet)
router.register('companies', CompaniesViewSet)
router.register('services', ServicesViewSet)
router.register('company-services', CompanyServicesViewSet)
router.register('itinerary', ItineraryViewSet)

urlpatterns = [
    path('apis/', include(router.urls))
]


